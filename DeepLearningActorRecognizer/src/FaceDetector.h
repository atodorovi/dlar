#pragma once
#include "Config.h"
#include "Image.h"
#include "Face.h"
#include <opencv2/dnn/dnn.hpp>

class FaceDetector
{
public:
	FaceDetector(Config& settings);
	std::vector<Face> getFaces(Image frame, float faceConfidence);
private:
	cv::dnn::Net net;
};