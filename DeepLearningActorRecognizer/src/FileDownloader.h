#pragma once
#include <string>
#include <opencv2/opencv.hpp>

class FileDownloader {
public:
	static std::string urlEncode(const std::string& input);
	static std::string downloadJSONstring(const std::string& url, int timeout = 10);
	static cv::Mat downloadImage(const std::string& url, int timeout = 10);
private:
	static size_t write_string(const char* in, size_t size, size_t num, std::string* out);
	static size_t write_image(const char* ptr, size_t size, size_t nmemb, void* userdata);
};