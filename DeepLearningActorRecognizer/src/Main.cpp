#include "Config.h"
#include <filesystem>
#include "Application.h"

bool filepathsInSettingsFound(Config& settings) {
	std::vector<std::string> filePaths;
	filePaths.push_back(settings.getString("actor_folder"));
	filePaths.push_back(settings.getString("video_output_folder"));
	filePaths.push_back(settings.getString("shape_predictor"));
	filePaths.push_back(settings.getString("face_recognition_network"));
	filePaths.push_back(settings.getString("face_detection_network_config"));
	filePaths.push_back(settings.getString("face_detection_network"));

	bool allFilesExist = true;
	for (const std::string& file : filePaths) {
		if (!std::filesystem::exists(file)) {
			std::cout << "Invalid filepath in settings.json. File or folder does not exist: " << file << std::endl;
			allFilesExist = false;
		}
	}

	return allFilesExist;
}

bool configurationFound(const std::string& workingDir) {
	const auto settingsPath = workingDir + "\\config\\settings.json";
	const auto parametersPath = workingDir + "\\config\\parameters.json";

	bool allFilesExist = true;

	if (std::filesystem::exists(settingsPath)) {
		std::cout << "[1/2] settings.json found: " << settingsPath << std::endl;
	}
	else {
		std::cout << "settings.json missing: " << settingsPath << std::endl;
		allFilesExist = false;
	}

	if (std::filesystem::exists(parametersPath)) {
		std::cout << "[2/2] parameters.json found: " << parametersPath << std::endl << std::endl;
	}
	else {
		std::cout << "parameters.json missing: " << parametersPath << std::endl;
		allFilesExist = false;
	}

	return allFilesExist;
}

int main(int argc, char** argv)
{
	const auto workingDir = std::filesystem::weakly_canonical(std::filesystem::path(argv[0])).parent_path().string();
	const auto settingsPath = workingDir + "\\config\\settings.json";
	const auto parametersPath = workingDir + "\\config\\parameters.json";

	if (!configurationFound(workingDir)) return -1;
	
	Config settings(settingsPath);
	Config parameters(parametersPath);

	if (!filepathsInSettingsFound(settings)) return -1;

	ActorManager actorManager(settings);
	ActorRecognizer actorRecognizer(settings, parameters);
	VideoPlayer player(actorRecognizer, settings);

	Application app(actorManager, actorRecognizer, player, settings, parameters);

	app.Run();

	return 0;
}