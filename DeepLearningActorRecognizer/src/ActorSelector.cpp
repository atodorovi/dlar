#include "ActorSelector.h"

ActorSelector::ActorSelector(TheMovieDatabase& movieDatabase) : movieDatabase(movieDatabase) {}

std::vector<Actor> ActorSelector::selectActors(Movie& movie)
{
	std::vector<Actor> cast = movieDatabase.getActors(movie.getId());
	std::cout << "Full cast of the movie " << movie.getTitle() << std::endl;
	int index = 1;
	for (Actor actor : cast) {
		std::cout << index++ << ". " << actor.getName() << std::endl;
	}

	std::vector<Actor> selectedActors;
	while (true) {
		std::cout << "Actor index: ";
		size_t actorIndex;
		std::cin >> actorIndex;
		if (actorIndex == 0) break;
		selectedActors.push_back(cast[actorIndex - 1]);
	}

	return selectedActors;
}
