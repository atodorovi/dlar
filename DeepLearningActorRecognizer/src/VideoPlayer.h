#pragma once
#include <string>
#include <opencv2/videoio.hpp>
#include "Image.h"
#include "ActorRecognizer.h"
#include "Config.h"

class VideoPlayer
{
public:
	VideoPlayer(ActorRecognizer& actorRecognizer, Config& settings);
	bool open(std::string videoPath);
	void play();
	void record(std::string output);
private:
	cv::VideoCapture videoCapture;
	double fps;
	double frameWidth;
	double frameHeight;
	bool showUnknown;
	bool showFrameNumber;
	std::string outputFolder;
	ActorRecognizer& actorRecognizer;
	Image getFrame();
	void drawActors(Image frame, std::vector<Face>& faces, bool drawUnknown);
};

