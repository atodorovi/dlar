#include "VideoPlayer.h"
#include "opencv2/highgui/highgui.hpp"

#include <fstream>
#include <cstdio>

VideoPlayer::VideoPlayer(ActorRecognizer& actorRecognizer, Config& settings) :
	actorRecognizer(actorRecognizer),
	showUnknown(settings.getBool("show_unknown_faces")),
	outputFolder(settings.getString("video_output_folder")),
	showFrameNumber(settings.getBool("show_frame_number")),
	fps(0.0),
	frameHeight(0.0),
	frameWidth(0.0)
{}

bool VideoPlayer::open(std::string videoPath)
{
	videoCapture.open(videoPath);
	bool opened = videoCapture.isOpened();
	if (opened) {
		fps = videoCapture.get(cv::CAP_PROP_FPS);
		frameWidth = videoCapture.get(cv::CAP_PROP_FRAME_WIDTH);
		frameHeight = videoCapture.get(cv::CAP_PROP_FRAME_HEIGHT);
	}

	return opened;
}

void VideoPlayer::play()
{
	int frameCounter = 1;

	while (1) {
		Image frame = getFrame();
		if (frame.GetMatrix().empty()) break;

		std::vector<Face> faces = actorRecognizer.recognizeActors(frame);
		drawActors(frame, faces, showUnknown);
		if(showFrameNumber) putText(frame.GetMatrix(), std::to_string(frameCounter), cv::Point(0, 20), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 255, 0), 2.0);

		cv::imshow("video", frame.GetMatrix());
		frameCounter++;

		char c = cv::waitKey(1);
		if (c == 27) break; // ESC exits the video
	}

	cv::destroyAllWindows();
}

void VideoPlayer::record(std::string output)
{
	cv::VideoWriter video(output, cv::VideoWriter::fourcc('m','p','4','v'), fps, cv::Size(frameWidth, frameHeight));

	int frameCounter = 1;

	while (1) {
		Image frame = getFrame();
		if (frame.GetMatrix().empty()) break;

		std::vector<Face> faces = actorRecognizer.recognizeActors(frame);
		drawActors(frame, faces, showUnknown);
		if (showFrameNumber) putText(frame.GetMatrix(), std::to_string(frameCounter), cv::Point(0, 20), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 255, 0), 2.0);
		video.write(frame.GetMatrix());
		frameCounter++;
	}

	video.release();
	cv::destroyAllWindows();
}

Image VideoPlayer::getFrame()
{
	cv::Mat m;
	videoCapture.read(m);
	return Image(m);
}

void VideoPlayer::drawActors(Image frame, std::vector<Face>& faces, bool drawUnknown) {
	for (auto& face : faces) {
		std::string labelText = face.getIdentity();
		std::string distanceText = cv::format("%.3f", face.getDistance());
		
		if (drawUnknown == false && face.getDistance() == -1) continue;

		// Calculate the position for annotated text (make sure we don't put illegal values in there):
		int distance_pos_x = std::max(face.GetRectangle().tl().x, 0);
		int distance_pos_y = std::max(face.GetRectangle().tl().y + face.GetRectangle().height + 25, 0);

		int label_pos_x = std::max(face.GetRectangle().tl().x, 0);
		int label_pos_y = std::max(face.GetRectangle().tl().y + face.GetRectangle().height + 50, 0);

		// Add text and rectangle to frame
		putText(frame.GetMatrix(), distanceText, cv::Point(distance_pos_x, distance_pos_y), cv::FONT_HERSHEY_SIMPLEX, 0.65, CV_RGB(0, 255, 0), 2.0);
		putText(frame.GetMatrix(), labelText, cv::Point(label_pos_x, label_pos_y), cv::FONT_HERSHEY_SIMPLEX, 0.65, CV_RGB(0, 255, 0), 2.0);
		cv::rectangle(frame.GetMatrix(), face.GetRectangle(), cv::Scalar(0, 255, 0), 1, 4);
	}
}