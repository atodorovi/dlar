#pragma once
#include "Config.h"
#include "Image.h"

class ActorDatabase {
public:
	enum class ImageChannel {
		GRAYSCALE = 0,
		ORIGINAL = -1
	};
	ActorDatabase(Config& settings);
	std::vector<Image> getActorImages(const std::string& actorName, ImageChannel channel = ImageChannel::ORIGINAL);
	void saveActorImages(std::vector<Image>& images, const std::string& actorName);
	bool actorFolderExists(const std::string& actorName);
private:
	const std::string actorFolder;
	static std::vector<std::string> getFilePaths(const std::string& folderPath);
};