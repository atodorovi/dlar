#include "ActorRecognizer.h"

ActorRecognizer::ActorRecognizer(Config& settings, Config& parameters) :
	faceCreator(settings),
	faceRecognizer(settings), 
	faceDetection(settings, parameters),
	actorCopies(parameters.getUInt64("actor_copies_per_image")),
	threshold(parameters.getDouble("face_recognition_threshold")) {}

void ActorRecognizer::learnActors(std::vector<Actor>& actors)
{
	faceCreator.createFaces(actors, actorCopies);
	faceRecognizer.train(actors);
}

std::vector<Face> ActorRecognizer::recognizeActors(Image frame)
{
	std::vector<Face> faces = faceDetection.getFaces(frame);
	if (faceDetection.isRecognitionRequired()) {
		faceRecognizer.recognizeActors(faceDetection.getDetectedFaces(), threshold);
		faceRecognizer.copyActorsIdentity(faceDetection.getDetectedFaces(), faces);
	}
	else {
		faceRecognizer.copyActorsIdentity(faceDetection.getDetectedFaces(), faces);
	}

	return faces;
}

const std::vector<std::string>& ActorRecognizer::getLearnedActors()
{
	return faceRecognizer.getUniqueLabels();
}
