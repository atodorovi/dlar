#include "Actor.h"

Actor::Actor(int id, const std::string& name) : id(id), name(name) {}

const int Actor::getId()
{
	return id;
}

const std::string Actor::getName()
{
	return name;
}

void Actor::appendImages(const std::vector<Image>& images)
{
	this->images.insert(this->images.end(), images.begin(), images.end());
}

std::vector<Image>& Actor::getImages()
{
	return images;
}

const int Actor::getNumberOfImages()
{
	return static_cast<int>(images.size());
}

std::vector<Face>& Actor::getFaces()
{
	return faces;
}

void Actor::appendFaces(const std::vector<Face>& faces)
{
	this->faces.insert(this->faces.end(), faces.begin(), faces.end());
}
