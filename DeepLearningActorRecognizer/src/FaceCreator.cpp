#include "FaceCreator.h"
#include "FaceVariator.h"
#include <dlib/image_transforms.h>

FaceCreator::FaceCreator(Config& settings) : faceExtractor(settings) {}

void FaceCreator::createFaces(std::vector<Actor>& actors, int n)
{
	for (auto& actor : actors) {
		std::vector<Face> images = createFaces(actor.getImages(), n);
		actor.appendFaces(images);
	}
}

Face FaceCreator::resizeFace(Face face, int width, int height)
{
	dlib::matrix<dlib::rgb_pixel> resizedFace(height, width);
	dlib::resize_image(face.GetMatrix(), resizedFace);

	return resizedFace;
}

std::vector<Face> FaceCreator::createFaces(std::vector<Image>& images, int n)
{
	std::vector<Face> faces = faceExtractor.extractFaces(images);
	if (n == 0) return faces;
	else {
		return FaceVariator::getFaceVariations(faces, n);
	}
}