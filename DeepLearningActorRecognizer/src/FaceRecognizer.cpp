#include "FaceRecognizer.h"
#include <dlib/opencv.h>
#include "FaceCreator.h"

FaceRecognizer::FaceRecognizer(Config& settings)
{
	dlib::deserialize(settings.getString("face_recognition_network")) >> net;
}

void FaceRecognizer::train(std::vector<Actor>& actors)
{
	// actor set is empty
	if (actors.empty()) {
		return;
	}

	// create face set
	std::vector<Face> faces;
	for (auto& actor : actors) {
		faces.insert(faces.end(), actor.getFaces().begin(), actor.getFaces().end());
		labels.insert(labels.end(), actor.getFaces().size(), actor.getName());
		uniqueLabels.emplace_back(actor.getName());
	}

	// check if all faces are the same size
	long image_size = faces[0].GetMatrix().size();

	bool is_same_size = std::all_of(faces.begin(), faces.end(), [&](Face& face) {
		return face.GetMatrix().size() == image_size;
		});

	// all faces are not the same size
	if (!is_same_size) {
		return;
	}

	std::vector<dlib::matrix<dlib::rgb_pixel>> f;
	for (auto& face : faces) {
		f.emplace_back(face.GetMatrix());
	}

	// creating 128d face embeddings
	faceDescriptors = net(f);
}

void FaceRecognizer::recognizeActors(std::vector<Face>& faces, float threshold)
{
	for (auto& face : faces) {
		recognizeActor(face, threshold);
	}
}

void FaceRecognizer::copyActorsIdentity(std::vector<Face>& srcImages, std::vector<Face>& dstImages)
{
	for (int i = 0; i < dstImages.size(); i++) {
		dstImages[i].setPrediction(srcImages[i].getDistance(), srcImages[i].getIdentity());
	}
}

const std::vector<std::string>& FaceRecognizer::getUniqueLabels()
{
	return uniqueLabels;
}

void FaceRecognizer::recognizeActor(Face& face, float threshold)
{
	// model is not trained
	if (faceDescriptors.empty()) {
		face.setPrediction(-1, "Unknown");
		return;
	}

	Face inputFace = FaceCreator::resizeFace(face, 150, 150);

	// face is not the same size as training faces
	// faces must be 150px x 150px = 22500px (neural network demands it)
	if (inputFace.GetMatrix().size() != 22500) {
		face.setPrediction(-1, "Unknown");
		return;
	}

	std::vector<std::pair<double, std::string>> predictions;

	auto descriptor = net(inputFace.GetMatrix());
	cv::Mat fdescriptor = dlib::toMat(descriptor);
	for (int i = 0; i < faceDescriptors.size(); i++) {
		cv::Mat tfdescriptor = dlib::toMat(faceDescriptors[i]);
		double distance = cv::norm(fdescriptor, tfdescriptor, cv::NORM_L2); // euclidean distance
		if (distance <= threshold) {
			predictions.push_back(std::pair<double, std::string>(distance, labels[i]));
		}
	}

	// no face found below threshold
	if (predictions.empty()) {
		face.setPrediction(-1, "Unknown");
		return;
	}

	// return face with lowest distance below threshold
	std::sort(predictions.begin(), predictions.end());

	face.setPrediction(predictions[0].first, predictions[0].second);
}
