#include "TheMovieDatabase.h"
#include "FileDownloader.h"

TheMovieDatabase::TheMovieDatabase(Config& settings) : 
	baseUrl(settings.getString("TMDB_base_url")),
	imageUrl(settings.getString("TMDB_image_url")),
	apiKey(settings.getString("TMDB_api_key"))
{}

std::vector<Movie> TheMovieDatabase::getMovies(std::string title)
{
	std::vector<Movie> movies;

	std::string encodedTitle = FileDownloader::urlEncode(title);
	std::string url = baseUrl + "search/movie?query=" + encodedTitle + "&api_key=" + apiKey;
	std::string jsonString = FileDownloader::downloadJSONstring(url);

	simdjson::padded_string json(jsonString);
	simdjson::ondemand::document doc = parser.iterate(json);

	for (simdjson::ondemand::object movie : doc["results"]) {
		const uint64_t id = movie["id"].get_uint64();
		const std::string overview(std::string_view(movie["overview"].get_string()));
		const std::string releaseDate(std::string_view(movie["release_date"].get_string()));
		const std::string title(std::string_view(movie["title"].get_string()));
		movies.emplace_back(id, title, overview, releaseDate);
	}

	return movies;
}

std::vector<Actor> TheMovieDatabase::getActors(int movieId)
{
	std::vector<Actor> actors;

	std::stringstream ss;
	ss << baseUrl << "movie/" << movieId << "/credits?api_key=" << apiKey;
	std::string url = ss.str();
	std::string jsonString = FileDownloader::downloadJSONstring(url);

	simdjson::padded_string json(jsonString);
	simdjson::ondemand::document doc = parser.iterate(json);

	for (simdjson::ondemand::object person : doc["cast"]) {
		if (person["cast_id"].get_uint64() != simdjson::NO_SUCH_FIELD) {
			const uint64_t id = person["id"].get_uint64();
			const std::string name(std::string_view(person["name"].get_string()));
			actors.emplace_back(id, name);
		}
	}

	return actors;
}

std::vector<Image> TheMovieDatabase::getActorImages(int actorId, ImageWidth width)
{
	std::vector<Image> images;

	std::vector<std::string> paths = getActorImageRelativePaths(actorId);
	std::string size;

	switch (width) {
	case ImageWidth::w300:
		size = "w300";
		break;
	case ImageWidth::w500:
		size = "w500";
		break;
	case ImageWidth::w780:
		size = "w780";
		break;
	case ImageWidth::w1280:
		size = "w1280";
		break;
	case ImageWidth::original:
		size = "original";
		break;
	}

	for (std::string& path : paths) {
		std::string url = imageUrl + size + path;
		cv::Mat img = FileDownloader::downloadImage(url);
		images.push_back(img);
	}

	return images;
}

std::vector<std::string> TheMovieDatabase::getActorImageRelativePaths(int actorId)
{
	std::vector<std::string> paths;

	std::stringstream ss;
	ss << baseUrl << "person/" << actorId << "/images?api_key=" << apiKey;
	std::string url = ss.str();
	std::string jsonString = FileDownloader::downloadJSONstring(url);

	simdjson::padded_string json(jsonString);
	simdjson::ondemand::document doc = parser.iterate(json);

	for (simdjson::ondemand::object image : doc["profiles"]) {
		const std::string path(std::string_view(image["file_path"].get_string()));
		paths.push_back(path);
	}

	return paths;
}
