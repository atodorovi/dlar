#pragma once
#include "FaceDetector.h"
#include "FaceTracker.h"
#include "FaceRecognizer.h"
#include "Config.h"

class FaceDetection
{
public:
	FaceDetection(Config& settings, Config& parameters, FaceTracker::TrackingAlgorithm algorithm = FaceTracker::TrackingAlgorithm::MedianFlow);
	std::vector<Face> getFaces(Image frame);
	bool isRecognitionRequired();
	std::vector<Face>& getDetectedFaces();
private:
	FaceDetector faceDetector;
	FaceTracker faceTracker;
	uint64_t skipFrames;
	float faceConfidence;
	int frameCounter;
	bool recognitionRequired;
	std::vector<Face> currentFaces;
};
