#include "ActorManager.h"

ActorManager::ActorManager(Config& settings) : actorDatabase(settings), movieDatabase(settings), actorSelector(movieDatabase), movieSelector(movieDatabase) {}

std::vector<Actor> ActorManager::selectActorsFromMovie()
{
	Movie movie = movieSelector.selectMovie();
	std::vector<Actor> actors = actorSelector.selectActors(movie);
	getActorImages(actors);

	return actors;
}

std::vector<Image> ActorManager::getActorImages(Actor& actor)
{
	std::vector<Image> images;
	bool exists = actorDatabase.actorFolderExists(actor.getName());
	if (exists) {
		images = actorDatabase.getActorImages(actor.getName());
		return images;
	}
	else {
		images = movieDatabase.getActorImages(actor.getId());
		actorDatabase.saveActorImages(images, actor.getName());
		return actorDatabase.getActorImages(actor.getName());
	}

	return images;
}

void ActorManager::getActorImages(std::vector<Actor>& actors)
{
	for (auto& actor : actors) {
		std::vector<Image> images = getActorImages(actor);
		actor.appendImages(images);
		std::cout << actor.getName() << " (" << actor.getNumberOfImages() << " images)" << std::endl;
	}
}
