#include "Face.h"
#include <dlib/opencv/cv_image.h>
#include <dlib/image_transforms.h>

Face::Face(dlib::matrix<dlib::rgb_pixel> matrix) : matrix(matrix), distance(-1), identity("Unknown") {}

Face::Face(dlib::matrix<dlib::rgb_pixel> matrix, cv::Rect rectangle) : matrix(matrix), rectangle(rectangle), distance(-1), identity("Unknown") {}

Face::Face(cv::Mat matrix, cv::Rect rectangle)
{
	dlib::cv_image<dlib::bgr_pixel> image(matrix);
	dlib::assign_image(this->matrix, image);
	this->rectangle = rectangle;
	distance = -1;
	identity = "Unknown";
}

dlib::matrix<dlib::rgb_pixel>& Face::GetMatrix()
{
	return matrix;
}

cv::Rect& Face::GetRectangle()
{
	return rectangle;
}

void Face::setIdentity(std::string identity)
{
	this->identity = identity;
}

void Face::setDistance(double distance)
{
	this->distance = distance;
}

void Face::setPrediction(double distance, std::string identity)
{
	this->distance = distance;
	this->identity = identity;
}

std::string Face::getIdentity()
{
	return identity;
}

double Face::getDistance()
{
	return distance;
}