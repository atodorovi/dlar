#pragma once
#include "Image.h"
#include "Face.h"
#include <opencv2/tracking.hpp>
#include <opencv2/tracking/tracking_legacy.hpp>

class FaceTracker
{
public:
	enum class TrackingAlgorithm {
		KCF, MOSSE, MedianFlow, CSRT
	};

	FaceTracker(TrackingAlgorithm algorithm);
	void trackFaces(Image frame, std::vector<Face> faces);
	bool foundAllFaces(Image frame);
	std::vector<Face> getFaces(Image frame);
private:
	cv::Ptr<cv::legacy::MultiTracker> multiTracker;
	TrackingAlgorithm algorithm;
};
