#pragma once
#include <dlib/matrix.h>
#include <opencv2/core/types.hpp>

class Face {
public:
	Face(dlib::matrix<dlib::rgb_pixel> matrix);
	Face(dlib::matrix<dlib::rgb_pixel> matrix, cv::Rect rectangle);
	Face(cv::Mat matrix, cv::Rect rectangle);
	dlib::matrix<dlib::rgb_pixel>& GetMatrix();
	cv::Rect& GetRectangle();
	void setIdentity(std::string identity);
	void setDistance(double distance);
	void setPrediction(double distance, std::string identity);
	std::string getIdentity();
	double getDistance();
private:
	dlib::matrix<dlib::rgb_pixel> matrix;
	cv::Rect rectangle;
	std::string identity;
	double distance;
};