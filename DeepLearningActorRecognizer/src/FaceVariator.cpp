#include "FaceVariator.h"
#include <dlib/image_transforms.h>

std::vector<Face> FaceVariator::getFaceVariations(std::vector<Face>& faces, int n)
{
	if (n == 0) return faces;
	else {
		thread_local dlib::rand rnd;

		std::vector<Face> faceVariations;
		for (auto& face : faces) {
			faceVariations.push_back(face); // push original face
			for (int i = 0; i < n; ++i) {
				faceVariations.push_back(dlib::jitter_image(face.GetMatrix(), rnd));
			}
		}

		return faceVariations;
	}
}
