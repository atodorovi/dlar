#pragma once
#include "Face.h"

class FaceVariator
{
public:
	static std::vector<Face> getFaceVariations(std::vector<Face>& faces, int n);
};