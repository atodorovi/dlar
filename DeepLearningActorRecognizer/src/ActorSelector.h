#pragma once
#include "TheMovieDatabase.h"
#include "Actor.h"

class ActorSelector {
public:
	ActorSelector(TheMovieDatabase& movieDatabase);
	std::vector<Actor> selectActors(Movie& movie);
private:
	TheMovieDatabase& movieDatabase;
};