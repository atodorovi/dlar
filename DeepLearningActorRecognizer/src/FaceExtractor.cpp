#include "FaceExtractor.h"
#include <dlib/opencv/cv_image.h>

FaceExtractor::FaceExtractor(Config& settings)
{
	dlib::deserialize(settings.getString("shape_predictor")) >> shapePredictor;
	faceDetector = dlib::get_frontal_face_detector();
}

std::vector<Face> FaceExtractor::extractFaces(std::vector<Image>& images)
{
	std::vector<Face> faces;

	for (auto& image : images) {
		// convert to dlib
		dlib::array2d<dlib::bgr_pixel> dlib_img;
		dlib::cv_image<dlib::bgr_pixel> cvImg(image.GetMatrix());
		dlib::assign_image(dlib_img, cvImg);

		// Now tell the face detector to give us a list of bounding boxes
		// around all the faces it can find in the image.
		std::vector<dlib::rectangle> detections = faceDetector(dlib_img);

		// no faces or multiple faces found
		if (detections.empty() || detections.size() > 1) {
			continue;
		}

		// find face landmarks
		std::vector<dlib::full_object_detection> shapes;
		for (auto& detection : detections)
		{
			dlib::full_object_detection shape = shapePredictor(dlib_img, detection);
			shapes.push_back(shape);
		}

		// We can also extract copies of each face that are cropped, rotated upright,
		// and scaled to a standard size as shown here:
		dlib::array<dlib::array2d<dlib::bgr_pixel> > face_chips;
		extract_image_chips(dlib_img, get_face_chip_details(shapes, 150), face_chips); // face must be 150 x 150 (neural network demands it)
		dlib::matrix<dlib::bgr_pixel> img = tile_images(face_chips);
		dlib::matrix<dlib::rgb_pixel> img_rgb;
		dlib::assign_image(img_rgb, img);

		faces.push_back(img_rgb);
	}

	return faces;
}
