#include "FaceTracker.h"

FaceTracker::FaceTracker(TrackingAlgorithm algorithm)
{
	multiTracker = cv::legacy::MultiTracker::create();
	this->algorithm = algorithm;
}

void FaceTracker::trackFaces(Image frame, std::vector<Face> faces)
{
	multiTracker = cv::legacy::MultiTracker::create();

	for (auto& face : faces) {
		multiTracker->add(cv::legacy::TrackerMedianFlow::create(), frame.GetMatrix(), face.GetRectangle());
	}
}

bool FaceTracker::foundAllFaces(Image frame) {
	return multiTracker->update(frame.GetMatrix());
}

std::vector<Face> FaceTracker::getFaces(Image frame)
{
	std::vector<Face> faces;

	int cols = frame.GetMatrix().cols;
	int rows = frame.GetMatrix().rows;
	std::vector<cv::Rect2d> faceRectangles = multiTracker->getObjects();
	for (auto& faceRectangle : faceRectangles) {
		// make sure that face rectangle is inside frame
		int x1 = faceRectangle.tl().x;
		int y1 = faceRectangle.tl().y;
		int x2 = faceRectangle.br().x;
		int y2 = faceRectangle.br().y;
		if (x1 < 0) x1 = 0;
		if (x1 > cols) x1 = cols;
		if (y1 < 0) y1 = 0;
		if (y1 > rows) y1 = rows;
		if (x2 < 0) x2 = 0;
		if (x2 > cols) x2 = cols;
		if (y2 < 0) y2 = 0;
		if (y2 > rows) y2 = rows;
		cv::Rect faceRect(cv::Point(x1, y1), cv::Point(x2, y2));
		if (faceRect.x >= 0 && faceRect.y >= 0 && faceRect.width + faceRect.x < cols && faceRect.height + faceRect.y < rows) {
			cv::Mat f = frame.GetMatrix()(faceRect);
			Face face(f.clone(), faceRect);
			faces.push_back(face);
		}
	}

	return faces;
}