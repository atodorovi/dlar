#include "MovieSelector.h"
#include <iostream>

MovieSelector::MovieSelector(TheMovieDatabase& movieDatabase) : movieDatabase(movieDatabase) {}

Movie MovieSelector::selectMovie()
{
	std::cout << "Enter movie title: ";
	std::string movieTitle;
	std::getline(std::cin, movieTitle);

	std::vector<Movie> movies = movieDatabase.getMovies(movieTitle);
	int index = 1;
	for (Movie movie : movies) {
		std::cout << index++ << ". " << movie.getTitle() << std::endl;
	}

	std::cout << "Enter movie index: ";
	size_t movieIndex;
	std::cin >> movieIndex;

	return movies[movieIndex - 1];
}
