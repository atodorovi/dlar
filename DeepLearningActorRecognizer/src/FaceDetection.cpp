#include "FaceDetection.h"

FaceDetection::FaceDetection(Config& settings, Config& parameters, FaceTracker::TrackingAlgorithm algorithm) :
	faceDetector(settings),
	faceTracker(algorithm),
	faceConfidence(parameters.getDouble("face_detection_confidence")),
	skipFrames(parameters.getUInt64("skip_frames")),
	frameCounter(0),
	recognitionRequired(true)
{}

std::vector<Face> FaceDetection::getFaces(Image frame)
{
	bool trackingFrame = frameCounter++ % (skipFrames + 1) != 0;
	bool foundAllFaces = true;

	if (trackingFrame) {
		foundAllFaces = faceTracker.foundAllFaces(frame);
		recognitionRequired = !foundAllFaces;
		if (foundAllFaces) {
			std::vector<Face> faces = faceTracker.getFaces(frame);
			recognitionRequired = false;
			return faces;
		}
	}
	if (!(trackingFrame && foundAllFaces)) {
		currentFaces = faceDetector.getFaces(frame, faceConfidence);
		faceTracker.trackFaces(frame, currentFaces);
		recognitionRequired = true;
		return currentFaces;
	}
}

bool FaceDetection::isRecognitionRequired()
{
	return recognitionRequired;
}

std::vector<Face>& FaceDetection::getDetectedFaces()
{
	return currentFaces;
}
