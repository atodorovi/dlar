#include "Application.h"
#include <chrono>

Application::Application(ActorManager& actorManager, ActorRecognizer& actorRecognizer, VideoPlayer& player, Config& settings, Config& parameters)
	: actorManager(actorManager), actorRecognizer(actorRecognizer), player(player), settings(settings), parameters(parameters), mode(1) {}

void Application::getMovieFile()
{
	std::cout << "Enter movie filename: ";
	std::getline(std::cin, filename);
}

void Application::getMode()
{
	std::cout << "Enter mode [1-play, 2-record]: ";
	std::cin >> mode;
}

std::vector<Actor> Application::getActorList()
{
	return actorManager.selectActorsFromMovie();
}

void Application::Run()
{
	// get movie path:
	getMovieFile();

	// get actors:
	std::vector<Actor> actors = getActorList();

	// get app mode:
	getMode();

	// train recognition:
	actorRecognizer.learnActors(actors);

	// output:
	if (player.open(filename)) {
		if (mode == 1) {
			player.play();
		}
		if (mode == 2) {
			long long unixTimestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
			std::stringstream ss;
			ss << settings.getString("video_output_folder") << "\\" << unixTimestamp << ".mp4";
			std::string outputFile = ss.str();

			player.record(outputFile);
		}
	}
}
