#pragma once
#include "Config.h"
#include "Actor.h"
#include "FaceExtractor.h"

class FaceCreator
{
public:
	FaceCreator(Config& settings);
	void createFaces(std::vector<Actor>& actors, int n);
	static Face resizeFace(Face face, int width, int height);
private:
	FaceExtractor faceExtractor;
	std::vector<Face> createFaces(std::vector<Image>& images, int n);
};