#pragma once
#include <string>
#include "Image.h"
#include "Face.h"

class Actor {
public:
	Actor(int id, const std::string& name);
	const int getId();
	const std::string getName();
	void appendImages(const std::vector<Image>& images);
	std::vector<Image>& getImages();
	const int getNumberOfImages();
	std::vector<Face>& getFaces();
	void appendFaces(const std::vector<Face>& faces);
private:
	const int id;
	const std::string name;
	std::vector<Image> images;
	std::vector<Face> faces;
};