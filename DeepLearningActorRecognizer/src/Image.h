#pragma once
#include <opencv2/core/mat.hpp>

class Image {
public:
	Image(cv::Mat matrix);
	cv::Mat& GetMatrix();
private:
	cv::Mat matrix;
};