#pragma once
#include "TheMovieDatabase.h"
#include "Movie.h"

class MovieSelector {
public:
	MovieSelector(TheMovieDatabase& movieDatabase);
	Movie selectMovie();
private:
	TheMovieDatabase& movieDatabase;
};