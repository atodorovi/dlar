#pragma once
#include "FaceCreator.h"
#include "FaceRecognizer.h"
#include "FaceDetection.h"
#include "Config.h"

class ActorRecognizer
{
public:
	ActorRecognizer(Config& settings, Config& parameters);
	void learnActors(std::vector<Actor>& actors);
	std::vector<Face> recognizeActors(Image frame);
	const std::vector<std::string>& getLearnedActors();
private:
	FaceCreator faceCreator;
	FaceRecognizer faceRecognizer;
	FaceDetection faceDetection;
	uint64_t actorCopies;
	float threshold;
};

