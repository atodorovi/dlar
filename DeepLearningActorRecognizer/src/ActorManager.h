#pragma once
#include "Config.h"
#include "ActorDatabase.h"
#include "TheMovieDatabase.h"
#include "ActorSelector.h"
#include "MovieSelector.h"

class ActorManager {
public:
	ActorManager(Config& settings);
	std::vector<Actor> selectActorsFromMovie();
private:
	ActorDatabase actorDatabase;
	TheMovieDatabase movieDatabase;
	ActorSelector actorSelector;
	MovieSelector movieSelector;
	std::vector<Image> getActorImages(Actor& actor);
	void getActorImages(std::vector<Actor>& actors);
};
