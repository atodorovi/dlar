#pragma once
#include <string>
#include "simdjson.h"

class Config {
public:
	Config(const std::string& filename);
	bool getBool(const std::string& key);
	double getDouble(const std::string& key);
	int64_t getInt64(const std::string& key);
	uint64_t getUInt64(const std::string& key);
	std::string getString(const std::string& key);
private:
	simdjson::ondemand::parser parser;
	simdjson::padded_string json;
	simdjson::ondemand::document doc;
};
