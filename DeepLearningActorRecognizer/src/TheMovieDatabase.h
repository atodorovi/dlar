#pragma once
#include "Config.h"
#include "Movie.h"
#include "Actor.h"

class TheMovieDatabase {
public:
	enum class ImageWidth {
		w300, w500, w780, w1280, original
	};
	TheMovieDatabase(Config& settings);
	std::vector<Movie> getMovies(std::string title);
	std::vector<Actor> getActors(int movieId);
	std::vector<Image> getActorImages(int actorId, ImageWidth width = ImageWidth::w780);
private:
	const std::string baseUrl;
	const std::string imageUrl;
	const std::string apiKey;
	simdjson::ondemand::parser parser;
	std::vector<std::string> getActorImageRelativePaths(int actorId);
};