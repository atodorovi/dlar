#pragma once
#include "ActorManager.h"
#include "ActorRecognizer.h"
#include "VideoPlayer.h"
#include "Config.h"
#include <string>

class Application
{
public:
	Application(ActorManager& actorManager, ActorRecognizer& actorRecognizer, VideoPlayer& player, Config& settings, Config& parameters);
	void getMovieFile();
	void getMode();
	std::vector<Actor> getActorList();
	void Run();
private:
	ActorManager& actorManager;
	ActorRecognizer& actorRecognizer;
	VideoPlayer& player;
	std::string filename;
	int mode;
	Config& parameters;
	Config& settings;
};
