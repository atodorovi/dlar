#pragma once
#include <string>

class Movie {
public:
	Movie(uint64_t id, const std::string& title, const std::string& overview, const std::string& releaseDate);
	const std::string getTitle();
	const uint64_t getId();
private:
	const uint64_t id;
	const std::string title;
	const std::string overview;
	const std::string releaseDate;
};