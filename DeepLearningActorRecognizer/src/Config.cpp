#include "Config.h"

Config::Config(const std::string& filename)
{
	json = simdjson::padded_string::load(filename);
	doc = parser.iterate(json);
}

bool Config::getBool(const std::string& key)
{
	return doc[key].get_bool();
}

double Config::getDouble(const std::string& key)
{
	return doc[key].get_double();
}

int64_t Config::getInt64(const std::string& key)
{
	return doc[key].get_int64();
}

uint64_t Config::getUInt64(const std::string& key)
{
	return doc[key].get_uint64();
}

std::string Config::getString(const std::string& key)
{
	return std::string(std::string_view(doc[key]));
}