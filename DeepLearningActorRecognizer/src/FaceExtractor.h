#pragma once
#include "Config.h"
#include "Face.h"
#include "Image.h"
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>

class FaceExtractor
{
public:
	FaceExtractor(Config& settings);
	std::vector<Face> extractFaces(std::vector<Image>& images);
private:
	dlib::shape_predictor shapePredictor;
	dlib::frontal_face_detector faceDetector;
};