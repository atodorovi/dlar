#include "FileDownloader.h"
#include <regex>
#include <curl/curl.h>
#include <opencv2/imgcodecs.hpp>

std::string FileDownloader::urlEncode(const std::string& input)
{
	return std::regex_replace(input, std::regex(R"(\s)"), "%20");
}

std::string FileDownloader::downloadJSONstring(const std::string& url, int timeout)
{
	const std::string http_url = std::regex_replace(url, std::regex("https"), "http"); // change protocol to HTTP

	std::string stream;
	CURL* curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_URL, http_url.c_str()); //the JSON url
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_string); // pass the writefunction
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &stream); // pass the stream ptr to the writefunction
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout); // timeout if curl_easy hangs, 
	curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); // use IPv4
	CURLcode res = curl_easy_perform(curl); // start curl
	curl_easy_cleanup(curl); // cleanup
	return stream;
}

cv::Mat FileDownloader::downloadImage(const std::string& url, int timeout)
{
	const std::string http_url = std::regex_replace(url, std::regex("https"), "http"); // change protocol to HTTP

	std::vector<uchar> stream;
	CURL* curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_URL, http_url.c_str()); //the img url
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_image); // pass the writefunction
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &stream); // pass the stream ptr to the writefunction
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout); // timeout if curl_easy hangs, 
	curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); // use IPv4
	CURLcode res = curl_easy_perform(curl); // start curl
	curl_easy_cleanup(curl); // cleanup
	return cv::imdecode(stream, -1); // 'keep-as-is'
}

size_t FileDownloader::write_string(const char* in, size_t size, size_t num, std::string* out)
{
	const size_t totalBytes(size * num);
	out->append(in, totalBytes);
	return totalBytes;
}

size_t FileDownloader::write_image(const char* ptr, size_t size, size_t nmemb, void* userdata)
{
	std::vector<uchar>* stream = (std::vector<uchar>*)userdata;
	const size_t count = size * nmemb;
	stream->insert(stream->end(), ptr, ptr + count);
	return count;
}
