#include "Movie.h"

Movie::Movie(uint64_t id, const std::string& title, const std::string& overview, const std::string& releaseDate) : id(id), title(title), overview(overview), releaseDate(releaseDate) {}

const std::string Movie::getTitle()
{
	return title;
}

const uint64_t Movie::getId()
{
	return id;
}
