#include "FaceDetector.h"

FaceDetector::FaceDetector(Config& settings)
{
	net = cv::dnn::readNetFromCaffe(settings.getString("face_detection_network_config"), settings.getString("face_detection_network"));
}

std::vector<Face> FaceDetector::getFaces(Image frame, float faceConfidence)
{
	std::vector<Face> faces;

	// Detect faces in the image
	cv::Mat inputBlob = cv::dnn::blobFromImage(frame.GetMatrix(), 1, cv::Size(600, 450), cv::Scalar(103.93, 116.77, 123.68));
	net.setInput(inputBlob, "data");
	cv::Mat detection = net.forward("detection_out");
	cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

	int cols = frame.GetMatrix().cols;
	int rows = frame.GetMatrix().rows;

	for (int r = 0; r < detectionMat.rows; r++)
	{
		float confidence = detectionMat.at<float>(r, 2);
		if (confidence > faceConfidence) {
			int x1 = static_cast<int>(detectionMat.at<float>(r, 3) * cols);
			int y1 = static_cast<int>(detectionMat.at<float>(r, 4) * rows);
			int x2 = static_cast<int>(detectionMat.at<float>(r, 5) * cols);
			int y2 = static_cast<int>(detectionMat.at<float>(r, 6) * rows);

			// make sure that face rectangle is inside frame
			if (x1 < 0) x1 = 0;
			if (x1 > cols) x1 = cols;
			if (y1 < 0) y1 = 0;
			if (y1 > rows) y1 = rows;
			if (x2 < 0) x2 = 0;
			if (x2 > cols) x2 = cols;
			if (y2 < 0) y2 = 0;
			if (y2 > rows) y2 = rows;
			cv::Rect faceRect(cv::Point(x1, y1), cv::Point(x2, y2));

			if (faceRect.x >= 0 && faceRect.y >= 0 && faceRect.width + faceRect.x < cols && faceRect.height + faceRect.y < rows) {
				cv::Mat f(frame.GetMatrix()(faceRect));;
				Face face(f.clone(), faceRect);
				faces.push_back(face);
			}
		}
	}

	return faces;
}
