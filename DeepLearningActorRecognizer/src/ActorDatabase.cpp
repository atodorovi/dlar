#include "ActorDatabase.h"
#include <opencv2/imgcodecs.hpp>
#include <filesystem>

ActorDatabase::ActorDatabase(Config& settings) : actorFolder(settings.getString("actor_folder")) {}

std::vector<Image> ActorDatabase::getActorImages(const std::string& actorName, ImageChannel channel)
{
	std::string folder = actorFolder + R"(\)" + actorName;
	std::vector<std::string> paths = getFilePaths(folder);

	std::vector<Image> images;

	// no files in folder
	if (paths.size() == 0) {
		return images;
	}

	int imageChannel = 0;
	switch (channel) {
	case ImageChannel::GRAYSCALE:
		imageChannel = 0;
		break;
	case ImageChannel::ORIGINAL:
		imageChannel = -1;
		break;
	}

	// load actor images from disk
	for (std::string& path : paths) {
		images.push_back(cv::imread(path, imageChannel));
	}

	return images;
}

void ActorDatabase::saveActorImages(std::vector<Image>& images, const std::string& actorName)
{
	// create specific actor folder
	std::string folder = actorFolder + R"(\)" + actorName;
	if (!std::filesystem::exists(folder)) {
		std::filesystem::create_directory(folder);
	}

	// save actor images to disk
	int index = 0;
	for (Image& image : images) {
		// save only images with 3 channels
		if (image.GetMatrix().channels()==3) {
			std::stringstream ss;
			ss << folder << R"(\)" << actorName << index++ << ".jpg";
			cv::imwrite(ss.str(), image.GetMatrix());
		}
	}
}

bool ActorDatabase::actorFolderExists(const std::string& actorName)
{
	std::string folder = actorFolder + R"(\)" + actorName;
	return std::filesystem::exists(folder);
}

std::vector<std::string> ActorDatabase::getFilePaths(const std::string& folderPath)
{
	std::vector<std::string>  paths;

	for (const auto& entry : std::filesystem::directory_iterator(folderPath)) {
		if (std::filesystem::is_regular_file(entry)) {
			paths.emplace_back(entry.path().generic_string());
		}
	}

	return paths;
}
